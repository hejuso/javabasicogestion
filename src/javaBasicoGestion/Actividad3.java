package javaBasicoGestion;

import java.util.Scanner;

public class Actividad3 {

	public static void main(String[] args) {

		Scanner introd = new Scanner(System.in);
		int numInt;
		int contador = 0;
		System.out.print("Empezamos el programa: \n\n");

		System.out.println("Introduce un numero:");
		// Comprobamos que el n�mero introducido sea un entero y no un String
		// Mientras que no sea un entero, mostramos un mensaje y le volvemos a pedir el
		// numero
		while (!introd.hasNextInt()) {
			introd.next();
			System.out.println("No has introducido un numero, vuelve a probar. \n");
		}

		numInt = introd.nextInt();
		numInt++;

		// CON ESTRUCTURA do-while
		System.out.println("CON ESTRUCTURA do-while \n");
		do {
			System.out.println(contador);
			contador++;
		} while ((contador != numInt));

		// CON ESTRUCTURA for
		System.out.println("\n CON ESTRUCTURA for \n");
		for (int i = 0; i < numInt; i++) {
			System.out.println(i);
		}
	}
}
