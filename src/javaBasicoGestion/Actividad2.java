package javaBasicoGestion;

import java.util.Scanner;

public class Actividad2 {

	public static void main(String[] args) {
		String[] diasSemana = { "", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "S�bado", "Domingo" };
		Scanner introd = new Scanner(System.in);
		int numInt;
		System.out.print("Empezamos el programa: \n\n");

		System.out.println("Introduce un numero del 1 al 7:");

		// Comprobamos que el n�mero introducido sea un entero y no un String
		// Mientras que no sea un entero, mostramos un mensaje y le volvemos a pedir el
		// numero
		while (!introd.hasNextInt()) {
			introd.next();
			System.out.println("No has introducido un numero, vuelve a probar. \n");
			System.out.println("Introduce un numero del 1 al 7:");
		}
		;
		numInt = introd.nextInt();

		// Comprobamos que el numero est� entre el 1 y el 7
		if (numInt > 7 || numInt < 1) {
			System.out.println("No has introducido un numero correcto, cerrando programa... \n");
		} else {
			// Recorremos el array y, si coincide el n�mero introducido con el contador,
			// mostramos el d�a de la semana correspondiente
			for (int i = 0; i < diasSemana.length; i++) {
				if (i == numInt) {
					System.out.println(diasSemana[numInt]);
				}
			}
		}

	}

}
