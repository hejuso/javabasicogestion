package javaBasicoGestion;

import java.util.Scanner;

public class Actividad1 {

	public static void main(String[] args) {

		Scanner introd = new Scanner(System.in);
		int num1;
		int num2;
		int num3;
		int resultado;
		System.out.print("Empezamos el programa: \n\n");

		System.out.print("Introduzca numero 1: ");
		// Comprobamos que el n�mero introducido sea un entero y no un String
		// Mientras que no sea un entero, mostramos un mensaje y le volvemos a pedir el
		// numero
		while (!introd.hasNextInt()) {
			introd.next();
			System.out.println("No has introducido un numero, vuelve a probar.");
		}
		;
		//Convertimos el n�mero recibido en int
		num1 = introd.nextInt();

		System.out.print("Introduzca numero 2: ");

		while (!introd.hasNextInt()) {
			introd.next();
			System.out.println("No has introducido un numero, vuelve a probar.");
		}
		;
		num2 = introd.nextInt();

		System.out.print("Introduzca numero 3: \n");

		while (!introd.hasNextInt()) {
			introd.next();
			System.out.println("No has introducido un numero, vuelve a probar.");
		}
		;
		num3 = introd.nextInt();

		//Si es positivo, multiplicamos, si es negativo, sumaremos
		if (num1 >= 0) {
			System.out.println("El primer numero es: positivo");
			resultado = num1 * num2 * num3;

		} else {
			System.out.println("El primer numero es: negativo");
			resultado = num1 + num2 + num3;
		}

		System.out.println("El resultado es: " + resultado);
	}

}
