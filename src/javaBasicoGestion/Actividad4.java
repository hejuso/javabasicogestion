package javaBasicoGestion;

import java.util.Scanner;
import java.util.Vector;

public class Actividad4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int vectorEnteros[] = new int[10];
		int introducido;
		int contador;

		// Recorremos el vector
		for (int i = 0; i < vectorEnteros.length; i++) {
			contador = 10 - i;
			System.out.println("Introduce " + contador + " numeros:");
			Scanner introd = new Scanner(System.in);
			// Comprobamos que el n�mero introducido sea un entero y no un String
			// Mientras que no sea un entero, mostramos un mensaje y le volvemos a pedir el
			// numero
			while (!introd.hasNextInt()) {
				introd.next();
				System.out.println("No has introducido un numero, vuelve a probar. \n");
			}

			// Almacenamos el n�mero introducido en el array
			introducido = introd.nextInt();
			vectorEnteros[i] = introducido;
		}

		System.out.println("\nResultado de lo introducido.");

		// Recorremos todo el array y mostramos su resultado.
		for (int i : vectorEnteros) {
			System.out.println(i);
		}
	}

}
